if (process.browser) {
  window.mixpanel = require('mixpanel-browser')
  console.log(process.env.mixpanelId)
  mixpanel.init(process.env.mixpanelId)

  mixpanel.track('user visited landing page')
}
