const images = {
    mockup: require('../assets/images/mockup.png'),
    team: require('../assets/images/collaboration.svg'),
    calc: require('../assets/images/calculator.svg'),
    calendar: require('../assets/images/calendar.svg'),
    logo: require('../assets/images/logo.png'),
    paperPlane: require('../assets/images/send.svg'),
    airplane: require('../assets/images/airplane.svg'),
    rocket: require('../assets/images/rocket.svg')
}

const pricings = [
    {
        icon: images.paperPlane,
        title: "Paper Plane",
        slug: "paper-plane",
        subtitle: "Ideal for the smaller businesses",
        price: "€19/month",
        hover: false,
        active: false,
        features: [
            {
                icon: "check_circle_outline",
                title: "Up to 10 employees",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Up to 2 admins",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Count hours worked"
            },
            {
                icon: "check_circle_outline",
                title: "Count holidays earned"
            },
            {
                icon: "check_circle_outline",
                title: "Notifications system"
            }
        ]
    },
    {
        icon: images.airplane,
        title: "Airplane",
        slug: "airplane",
        subtitle: "Ideal for the medium businesses",
        price: "€29/month",
        hover: false,
        active: false,
        features: [
            {
                icon: "check_circle_outline",
                title: "Up to 25 employees",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Up to 5 admins",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Count hours worked"
            },
            {
                icon: "check_circle_outline",
                title: "Count holidays earned"
            },
            {
                icon: "check_circle_outline",
                title: "Notifications system"
            }
        ]
    },
    {
        icon: images.rocket,
        title: "Rocket",
        slug: "rocket",
        subtitle: "Ideal for the bigger businesses",
        price: "€49/month",
        hover: false,
        active: false,
        features: [
            {
                icon: "check_circle_outline",
                title: "Up to 50 employees",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Up to 10 admins",
                bold: true
            },
            {
                icon: "check_circle_outline",
                title: "Count hours worked"
            },
            {
                icon: "check_circle_outline",
                title: "Count holidays earned"
            },
            {
                icon: "check_circle_outline",
                title: "Notifications system"
            }
        ]
    }
]

const features = [
    {
        image: images.team,
        title: 'Teams',
        body: 'Toorno allows you to assign each employee to a specific team giving you the opportunity of have a much more clear overview'
    },
    {
        image: images.calendar,
        title: 'Quick Scheduling',
        body: 'Toorno will allow you to quickly arrange your staff to work over multiple days reducing the time it takes to prepare the schedule'
    },
    {
        image: images.calc,
        title: 'Hours And Vacation',
        body: 'Toorno will automatically add up the hours worked and the holidays earned by your staff and keep them available for you to access at any moment'
    }
]

export { images, pricings, features}