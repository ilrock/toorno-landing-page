const functions = require('firebase-functions');
const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser')
const axios = require('axios')

axios.defaults.headers.common['X-MailerLite-ApiKey'] = '06cd5aa77224d735a11fdd744feb6625'
axios.defaults.headers.common['Content-Type'] = 'application/json'

const groups = [
  {
    name: 'Airplane',
    id: '10426824'
  },
  {
    name: 'Rocket',
    id: '10426826'
  },
  {
    name: 'PaperPlane',
    id: '10426822'
  }
]

app.use(cors({ origin: true }));
app.use(bodyParser.json())

app.get('/test', (req, res) => {
  console.log("hello from firebase")
  res.send("Hello from firebase")
})

app.post('/mailerliteGroups', (req, res) => {
  const user = req.body.user
  const plan = req.body.selectedPlan

  const groupId = groups.find((group) => group.name === plan).id
  const url = `http://api.mailerlite.com/api/v2/groups/${groupId}/subscribers`
  
  axios.post(url, {
    email: user.email,
    name: user.name,
  })
  .then((response) => res.status(200).send({
    message: "The user has been created"
  }))
  .catch((error) => res.status(500).send({
    message: "Something went wrong"
  }))
})

exports.api = functions.https.onRequest(app);
